class Animal:
    def __init__(self, name, age, legs):
        self.name = name
        self.age = age
        self.legs = legs
    
    def die(self):
        print('I died')
    
    def strl(self):
        return str(self.name)
    
    def __str__(self):
        return str(self.name)
    
class Human(Animal): # Inherit
    def __init__(self, name, age):
        super().__init__(name, age, 2)

    def work(self):
        print('I work')

if __name__ == '__main__':
    ani = Animal('Book', 35, 4)
    print(ani.strl())
    print(str(ani))